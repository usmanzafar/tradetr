<?php
/*
 * Just a basic route responsible for loading methods as per the controllers
 */
$controllers = array('pages' => ['home', 'error']);

if (array_key_exists($controller, $controllers)) {
    url_mapper($controller, $action);

} else {
    url_mapper('pages', 'error');
}





