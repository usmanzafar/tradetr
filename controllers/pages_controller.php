<?php
require_once ('models/TradeTracker.php');

class PagesController {
    public function home() {

        ob_start();
        include 'views/pages/home.php';
        $data = ob_get_clean(); //Todo : USe functions to take care of buffers

        require_once('views/template.php');
    }

    public function process()
    {
        global $path;
        global $tag;

        if(!isset($_GET['urlbox'])) {
            echo json_encode(array("message" => "url not set"));
            die();
        }
        $url = urldecode($_GET['urlbox']);
        //$url = 'http://pf.tradetracker.net/?aid=1&type=xml&encoding=utf-8&fid=251713&categoryType=2&additionalType=2&limit=60000';

        $config = [
            'url' => $url,
            'path'=> $path,
            'tag' => $tag

        ];

        try {
            $trade_tracker = new TradeTracker($config);
            $total_products =  $trade_tracker->loadFromURL();
            $response = ['message' => "Total records processed are : ".$total_products." All downloaded in: ".$path];
        }
        catch(Exception $ex)
        {
            $response = ['message' => $ex->getMessage()];
        }

        echo json_encode($response);
    }

    public function error() {
        $data = file_get_contents('views/pages/error.php');
        require_once('views/template.php');
    }
}

