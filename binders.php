<?php


/*
 * Uncomment this if you want to see system errors
 *
 * */
function shutdown()
{
    $a=error_get_last();
    if($a !== null)
        echo json_encode($a);
}
register_shutdown_function('shutdown');
ini_set("display_errors", 0);
