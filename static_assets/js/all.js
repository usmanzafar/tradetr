document.addEventListener('DOMContentLoaded', function() {
    var btn = document.getElementById("btn_submit");
    var urlbox = document.getElementById('urlbox');
    var status = document.getElementById('status');
    var loader = document.getElementById('loader');

    btn.addEventListener('click',function(event){
        event.preventDefault();
        var current_url = window.location.href;


        if (   ValidURL(urlbox.value) === false )
            return false;


        console.log(current_url);
        //current_url = current_url.substring(0,current_url.indexOf("?"));


        var url = current_url+'?controller=pages&action=process&urlbox='+encodeURIComponent(urlbox.value);
        console.log(url);

        var http_request = new XMLHttpRequest();
        try{
            http_request = new XMLHttpRequest();
        }catch (e){
            // IE
            try{
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    alert("Your browser cannot handle ajax stuff!, this test project is cool but not that cool " +
                    "YET!!");
                    return false;
                }
            }
        }
        http_request.onreadystatechange  = function(){
            if (http_request.readyState == 4  )
            {
                var responseObj = JSON.parse(http_request.responseText);
                loader.style.display = 'none';
                console.log(responseObj);
                status.innerHTML = responseObj.message;
            }
            else
            {
                loader.style.display = 'block';
            }

        }
        http_request.open("GET", url, true);
        http_request.send();


    });


}, false);


/*
Checks if url is empty or not
 */
function ValidURL(str) {
    if(str.length == 0)
    {
        alert ('URL is empty, try valid url of xml feed');
        return false;
    }

    /*
     Todo: Add further validation to ensure its a url only
     */

}
