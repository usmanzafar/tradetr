<?php
if(!function_exists("url_mapper")) {
    function url_mapper($controller, $action) {
        //die('controllers/' . $controller . '_controller.php');

        require_once('controllers/' . $controller . '_controller.php');

        // create a new instance of the needed controller
        switch($controller) {
            case 'pages':
                $controller = new PagesController();
                break;
        }

        // call the action
        $controller->{ $action }();
    }

}


if(!function_exists("url_getCurrentURL")) {
    function url_getCurrentURL() {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

}


