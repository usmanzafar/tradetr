<?php
/*
 * Generic Database Methods will come here
 */
class Model {

    /**
     * Save method that will dump as per the filename and data provided
     * @param string $file_name  data of the file
     * @param string $file_data  data of the file
     */
    protected function save($file_name, $file_data)
    {
        $dir = dirname($file_name);
        if (!is_dir($dir))
        {
            mkdir($dir, 0755, true);
        }

        $file = fopen($file_name,"w") or die('File could not be written');
        fwrite($file,$file_data);
        fclose($file);

    }

    /**
     * Validates the URL, checks if the domain requested in whitelisted or now
     * @param string $url
     * @param array $allowed
     *
     * @throws Exception
     * todo : validate the parameters as well
     */
    protected function validate($url,$allowed=array('pf.tradetracker.net','tradetracker.net'))
    {
        $url_requested = parse_url($url);
        $host_current = $url_requested['host'];
        if (!in_array($host_current,$allowed))
            throw new \Exception("Only these hosts are allowed: ".implode(",",$allowed));

    }

}
