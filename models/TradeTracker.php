<?php
/*
 * Main TradeTracker class responsible for fetching and storing the data
 */
require_once 'AffiliateType.php';
require_once 'Model.php';


class TradeTracker extends Model implements AffiliateType {

    protected $xml;
    protected $reader;
    protected $response;
    protected $tag;
    protected $path;

    public function __construct($config)
    {

        if(!is_array($config))
            throw new Exception('Parameter should be array having url,tag option');
        else
        {
            $this->xml  = $config['url'];
            $this->tag  = $config['tag'];
            $this->path = $config['path'];
            $this->reader = new XMLReader(); //using xmlreader for easier to manage code, alternative will be xmlparser
        }


    }


    /**
     * Set the xml url
     * @param $url
     */
    public function setXmlURL($url)
    {
        $this->xml = $url;
    }


    /**
     * Retrieves the xml or throws exception if not set
     * @return mixed
     * @throws Exception
     */
    public function getXmlURL()
    {
        if(empty($this->xml))
            throw new \Exception("URL is empty");
        $this->validate($this->xml);
        return $this->xml;
    }


    /**
     * Load the xml via XMLReader (pull parsing), similar to SAX but much easy to work with (efficeny are almost same)
     * Each node is processed as xmltree which makes it easier to work with
     * @return int returns the total products
     *
     */
    public function loadFromURL()
    {
        $this->xml = $this->getXmlURL();
        $this->reader->open($this->xml);
        while ($this->reader->read() && $this->reader->name !== (string)$this->tag);//move directly to tags
            $total_product = 0;
            while ($this->reader->name === (string)$this->tag) {
                $node = new SimpleXMLElement($this->reader->readOuterXML());
                $this->process($node);
                $total_product++;
                $this->reader->next((string)$this->tag);
            }
        return $total_product;

    }

    public function process($node)
    {
        if (get_class($node) === 'SimpleXMLElement') {
            $data = 'Product: ' . $node->name . '(' . $node->productID . ')' . "\n";
            $data .= 'Description: ' . $node->description . "\n";
            $data .= 'Price: ' . $node->price->attributes()['currency'] . ' ' . $node->price . "\n";
            $data .= 'Categories: ';

            foreach ($node->categories->category as $cat_data) {
                $category[] = (string)$cat_data;
            }

            $data .= implode(',', $category) . "\n";
            $data .= 'URL: ' . $node->productURL;
            $clean_product_id = preg_replace("/[^a-zA-Z0-9]+/", "_", $node->productID);
            $filename = $this->path.date('dmYHi').$clean_product_id.'.txt';

            $this->save($filename,$data);

        }
    }


}
