<?php
interface AffiliateType {

    function loadFromURL();
    function process($data);

}
